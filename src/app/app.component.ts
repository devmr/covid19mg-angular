import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavItem } from './models/navitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular';

  mainListItems: NavItem [] = [];
  secondaryListItems: NavItem [] = [];
  settingsListItems: NavItem [] = [];

  constructor( private router: Router) {

    this.mainListItems = [
      {
        name: 'Dashboard',
        icon: 'dashboard',
        route: '/'
      },
      {
        name:'Cases Types' ,
        icon: 'storage',
        route: '/casetype'
      },
    ];

    this.secondaryListItems = [
      {
        name:'Provinces' ,
        icon: 'places',
        route: '/provinces'
      },
      {
        name:'Régions' ,
        icon: 'places',
        route: '/regions'
      },
      {
        name: 'Districts',
        icon: 'places',
        route: '/districts'
      },
      {
        name: 'Communes',
        icon: 'places',
        route: '/communes'
      },
      ];

    this.settingsListItems = [
      {
        name: 'Settings',
        icon: 'settings',
        route: '/settings'
      }
    ];
  }
}
