import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import {CasetypeComponent} from "./views/casetype/casetype.component";
import {ProvincesComponent} from "./views/provinces/provinces.component";
import {RegionsComponent} from "./views/regions/regions.component";
import {DistrictsComponent} from "./views/districts/districts.component";
import {CommunesComponent} from "./views/communes/communes.component";
import {SettingsComponent} from "./views/settings/settings.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'casetype',
    component: CasetypeComponent
  },
  {
    path: 'provinces',
    component: ProvincesComponent
  },
  {
    path: 'regions',
    component: RegionsComponent
  },
  {
    path: 'districts',
    component: DistrictsComponent
  },
  {
    path: 'communes',
    component: CommunesComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
