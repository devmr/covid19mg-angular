import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasetypeComponent } from './casetype.component';

describe('CasetypeComponent', () => {
  let component: CasetypeComponent;
  let fixture: ComponentFixture<CasetypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasetypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
